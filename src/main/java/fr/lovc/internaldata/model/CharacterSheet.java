package fr.lovc.internaldata.model;

public record CharacterSheet(String botPrefix, String userPrefix, String description, String botVoice) {
}