package fr.lovc.textgen.model.output;

public class KoboldAiGenResult {
	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
}
